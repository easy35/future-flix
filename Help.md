
**future_flix**


**简介**：<p>科幻电影网站</p>


**HOST**:localhost:8888

**联系人**:11

**Version**:1.0

**接口路径**：/v2/api-docs

# tiezi-controller


## 获取全部帖子,默认分页一页10个帖子


**接口描述**:


**接口地址**:`/tiezi/all`


**请求方式**：`POST`


**consumes**:`["application/json"]`


**produces**:`["*/*"]`



**请求参数**：
暂无



**响应示例**:

```json
{
	"code": 0,
	"data": {},
	"errorMessage": "",
	"host": ""
}
```

**响应参数**:


| 参数名称         | 参数说明                             |    类型 |  schema |
| ------------ | -------------------|-------|----------- |
|code| 返回值代码  |integer(int32)  | integer(int32)   |
|data|   |object  |    |
|errorMessage| 错误信息  |string  |    |
|host|   |string  |    |





**响应状态**:


| 状态码         | 说明                            |    schema                         |
| ------------ | -------------------------------- |---------------------- |
| 200 | OK  |ResponseResult|
| 201 | Created  ||
| 401 | Unauthorized  ||
| 403 | Forbidden  ||
| 404 | Not Found  ||
## 删除帖子


**接口描述**:


**接口地址**:`/tiezi/del`


**请求方式**：`POST`


**consumes**:`["application/json"]`


**produces**:`["*/*"]`



**请求参数**：

| 参数名称         | 参数说明     |     in |  是否必须      |  数据类型  |  schema  |
| ------------ | -------------------------------- |-----------|--------|----|--- |
|id| 帖子id  | body | true |integer  |    |

**响应示例**:

```json
{
	"code": 0,
	"data": {},
	"errorMessage": "",
	"host": ""
}
```

**响应参数**:


| 参数名称         | 参数说明                             |    类型 |  schema |
| ------------ | -------------------|-------|----------- |
|code| 返回值代码  |integer(int32)  | integer(int32)   |
|data|   |object  |    |
|errorMessage| 错误信息  |string  |    |
|host|   |string  |    |





**响应状态**:


| 状态码         | 说明                            |    schema                         |
| ------------ | -------------------------------- |---------------------- |
| 200 | OK  |ResponseResult|
| 201 | Created  ||
| 401 | Unauthorized  ||
| 403 | Forbidden  ||
| 404 | Not Found  ||
| 501 | 无效参数  ||
## 查看帖子详情


**接口描述**:


**接口地址**:`/tiezi/detail`


**请求方式**：`POST`


**consumes**:`["application/json"]`


**produces**:`["*/*"]`



**请求参数**：

| 参数名称         | 参数说明     |     in |  是否必须      |  数据类型  |  schema  |
| ------------ | -------------------------------- |-----------|--------|----|--- |
|id| id  | body | true |integer  |    |

**响应示例**:

```json
{
	"code": 0,
	"data": {},
	"errorMessage": "",
	"host": ""
}
```

**响应参数**:


| 参数名称         | 参数说明                             |    类型 |  schema |
| ------------ | -------------------|-------|----------- |
|code| 返回值代码  |integer(int32)  | integer(int32)   |
|data|   |object  |    |
|errorMessage| 错误信息  |string  |    |
|host|   |string  |    |





**响应状态**:


| 状态码         | 说明                            |    schema                         |
| ------------ | -------------------------------- |---------------------- |
| 200 | OK  |ResponseResult|
| 201 | Created  ||
| 401 | Unauthorized  ||
| 403 | Forbidden  ||
| 404 | Not Found  ||
| 501 | 无效参数  ||
## 分页带条件搜索


**接口描述**:


**接口地址**:`/tiezi/search`


**请求方式**：`POST`


**consumes**:`["application/json"]`


**produces**:`["*/*"]`


**请求示例**：
```json
{
	"page": 0,
	"qid": 0,
	"size": 0,
	"tieziState": 0,
	"title": "",
	"uid": 0
}
```


**请求参数**：

| 参数名称         | 参数说明     |     in |  是否必须      |  数据类型  |  schema  |
| ------------ | -------------------------------- |-----------|--------|----|--- |
|dto| 帖子分页和条件查询参数类  | body | true |TieziPageDto  | TieziPageDto   |

**schema属性说明**



**TieziPageDto**

| 参数名称         | 参数说明     |     in |  是否必须      |  数据类型  |  schema  |
| ------------ | -------------------------------- |-----------|--------|----|--- |
|page| 页码  | body | false |integer(int32)  |    |
|qid| 圈子id  | body | false |integer(int32)  |    |
|size| 每页容量  | body | false |integer(int32)  |    |
|tieziState| 帖子状态   | body | false |integer(int32)  |    |
|title| 标题  | body | false |string  |    |
|uid| 用户id  | body | false |integer(int32)  |    |

**响应示例**:

```json
{
	"code": 0,
	"data": {},
	"errorMessage": "",
	"host": ""
}
```

**响应参数**:


| 参数名称         | 参数说明                             |    类型 |  schema |
| ------------ | -------------------|-------|----------- |
|code| 返回值代码  |integer(int32)  | integer(int32)   |
|data|   |object  |    |
|errorMessage| 错误信息  |string  |    |
|host|   |string  |    |





**响应状态**:


| 状态码         | 说明                            |    schema                         |
| ------------ | -------------------------------- |---------------------- |
| 200 | OK  |ResponseResult|
| 201 | Created  ||
| 401 | Unauthorized  ||
| 403 | Forbidden  ||
| 404 | Not Found  ||
## 设置帖子状态


**接口描述**:


**接口地址**:`/tiezi/setStatus/{id}`


**请求方式**：`PUT`


**consumes**:`["application/json"]`


**produces**:`["*/*"]`



**请求参数**：

| 参数名称         | 参数说明     |     in |  是否必须      |  数据类型  |  schema  |
| ------------ | -------------------------------- |-----------|--------|----|--- |
|tid| 帖子id  | path | true |integer  |    |

**响应示例**:

```json
{
  "code": 0,
  "data": {},
  "errorMessage": "",
  "host": ""
}
```

**响应参数**:


| 参数名称         | 参数说明                             |    类型 |  schema |
| ------------ | -------------------|-------|----------- |
|code| 返回值代码  |integer(int32)  | integer(int32)   |
|data|   |object  |    |
|errorMessage| 错误信息  |string  |    |
|host|   |string  |    |





**响应状态**:


| 状态码         | 说明                            |    schema                         |
| ------------ | -------------------------------- |---------------------- |
| 200 | OK  |ResponseResult|
| 201 | Created  ||
| 401 | Unauthorized  ||
| 403 | Forbidden  ||
| 404 | Not Found  ||
| 501 | 无效参数  ||
## 提交帖子


**接口描述**:


**接口地址**:`/tiezi/submit`


**请求方式**：`POST`


**consumes**:`["application/json"]`


**produces**:`["*/*"]`


**请求示例**：
```json
{
  "qid": 0,
  "tid": 0,
  "tieziContent": "",
  "tieziState": 0,
  "title": "",
  "uid": 0
}
```


**请求参数**：

| 参数名称         | 参数说明     |     in |  是否必须      |  数据类型  |  schema  |
| ------------ | -------------------------------- |-----------|--------|----|--- |
|dto| TieziDto帖子前后端交互实体类  | body | true |TieziDto  | TieziDto   |

**schema属性说明**



**TieziDto**

| 参数名称         | 参数说明     |     in |  是否必须      |  数据类型  |  schema  |
| ------------ | -------------------------------- |-----------|--------|----|--- |
|qid| 圈子id  | body | false |integer(int32)  |    |
|tid| 帖子id  | body | false |integer(int32)  |    |
|tieziContent| 帖子内容  | body | true |string  |    |
|tieziState| 帖子状态   | body | false |integer(int32)  |    |
|title| 标题  | body | true |string  |    |
|uid| 用户id  | body | true |integer(int32)  |    |

**响应示例**:

```json
{
  "code": 0,
  "data": {},
  "errorMessage": "",
  "host": ""
}
```

**响应参数**:


| 参数名称         | 参数说明                             |    类型 |  schema |
| ------------ | -------------------|-------|----------- |
|code| 返回值代码  |integer(int32)  | integer(int32)   |
|data|   |object  |    |
|errorMessage| 错误信息  |string  |    |
|host|   |string  |    |





**响应状态**:


| 状态码         | 说明                            |    schema                         |
| ------------ | -------------------------------- |---------------------- |
| 200 | OK  |ResponseResult|
| 201 | 操作失败  ||
| 401 | Unauthorized  ||
| 403 | Forbidden  ||
| 404 | Not Found  ||
| 501 | 无效参数  ||
## 上传图片


**接口描述**:


**接口地址**:`/tiezi/upload`


**请求方式**：`POST`


**consumes**:`["multipart/form-data"]`


**produces**:`["*/*"]`



**请求参数**：

| 参数名称         | 参数说明     |     in |  是否必须      |  数据类型  |  schema  |
| ------------ | -------------------------------- |-----------|--------|----|--- |
|multipartFile| multipartFile  | body | false |string  |    |

**响应示例**:

```json
{
  "code": 0,
  "data": {},
  "errorMessage": "",
  "host": ""
}
```

**响应参数**:


| 参数名称         | 参数说明                             |    类型 |  schema |
| ------------ | -------------------|-------|----------- |
|code| 返回值代码  |integer(int32)  | integer(int32)   |
|data|   |object  |    |
|errorMessage| 错误信息  |string  |    |
|host|   |string  |    |





**响应状态**:


| 状态码         | 说明                            |    schema                         |
| ------------ | -------------------------------- |---------------------- |
| 200 | OK  |ResponseResult|
| 201 | Created  ||
| 401 | Unauthorized  ||
| 403 | Forbidden  ||
| 404 | Not Found  ||
