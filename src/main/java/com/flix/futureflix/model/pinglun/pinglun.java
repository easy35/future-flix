package com.flix.futureflix.model.pinglun;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author h
 * @since 2024-05-10
 */
public class pinglun implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "pid", type = IdType.AUTO)
    private Integer pid;
    @ApiModelProperty(value = "帖子id",required = true)
    private Integer tid;
    @ApiModelProperty(value = "用户id",required = true)
    private Integer uid;
    @ApiModelProperty(value = "评论内容",required = true)
    private String pinglunContent;

    private Integer dianzanNum;

    private Integer pinglunState;


    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public Integer getTid() {
        return tid;
    }

    public void setTid(Integer tid) {
        this.tid = tid;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public String getPinglunContent() {
        return pinglunContent;
    }

    public void setPinglunContent(String pinglunContent) {
        this.pinglunContent = pinglunContent;
    }

    public Integer getDianzanNum() {
        return dianzanNum;
    }

    public void setDianzanNum(Integer dianzanNum) {
        this.dianzanNum = dianzanNum;
    }

    public Integer getPinglunState() {
        return pinglunState;
    }

    public void setPinglunState(Integer pinglunState) {
        this.pinglunState = pinglunState;
    }

    @Override
    public String toString() {
        return "Pinglun{" +
                "pid=" + pid +
                ", tid=" + tid +
                ", uid=" + uid +
                ", pinglunContent=" + pinglunContent +
                ", dianzanNum=" + dianzanNum +
                ", pinglunState=" + pinglunState +
                "}";
    }
}
