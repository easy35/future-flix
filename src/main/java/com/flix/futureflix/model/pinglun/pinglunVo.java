package com.flix.futureflix.model.pinglun;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class pinglunVo {
    private Integer pid;
    @ApiModelProperty(value = "用户id")
    private Integer uid;

    @ApiModelProperty("用户名")
    private String uname;

    @ApiModelProperty("用户头像地址")
    private String userImg;

    @ApiModelProperty(value = "评论内容")
    private String pinglunContent;

    private Integer dianzanNum;

    private Integer pinglunState;
}
