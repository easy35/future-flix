package com.flix.futureflix.model.pinglun;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel( description= "pinglunDto帖子前后端交互实体类")
public class pinglunDto {
    @ApiModelProperty(value = "帖子id",required = true)
    private Integer tid;
    @ApiModelProperty(value = "用户id(正在使用网页的用户)",required = true)
    private Integer uid;
}
