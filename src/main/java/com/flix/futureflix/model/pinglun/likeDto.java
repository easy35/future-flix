package com.flix.futureflix.model.pinglun;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel( description= "likeDto帖子前后端交互实体类(专门用于处理点赞/取消点赞情况)")
public class likeDto {
    @ApiModelProperty(value = "用户id" ,required = true)
    private Integer uid;
    @ApiModelProperty(value = "评论id" ,required = true)
    private Integer pid;
    @ApiModelProperty(value = "点赞状态",required = true)
    private boolean liked;
}
