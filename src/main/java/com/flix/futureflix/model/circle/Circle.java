package com.flix.futureflix.model.circle;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@TableName("quanzi")
public class Circle implements Serializable{
    private static final long serialVersionUID = 1L;

    @TableId(value = "qid",type = IdType.AUTO)
    private Integer id;

    @TableField("qname")
    private String name;
    private String imgUrl;
    @TableField("quanzi_time")
    private Date createTime;
}

