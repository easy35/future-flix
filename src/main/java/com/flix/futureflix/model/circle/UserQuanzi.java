package com.flix.futureflix.model.circle;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;

import java.time.LocalDateTime;
import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author h
 * @since 2024-05-06
 */
public class UserQuanzi implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "uq_id", type = IdType.AUTO)
    private Integer uqId;

    @ApiModelProperty(value = "用户id",required = true)
    private Integer uid;

    @ApiModelProperty(value = "圈子id",required = true)
    private Integer qid;


    private LocalDateTime joinTime;


    public Integer getUqId() {
        return uqId;
    }

    public void setUqId(Integer uqId) {
        this.uqId = uqId;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public Integer getQid() {
        return qid;
    }

    public void setQid(Integer qid) {
        this.qid = qid;
    }

    public LocalDateTime getJoinTime() {
        return joinTime;
    }

    public void setJoinTime(LocalDateTime joinTime) {
        this.joinTime = joinTime;
    }

    @Override
    public String toString() {
        return "UserQuanzi{" +
                "uqId=" + uqId +
                ", uid=" + uid +
                ", qid=" + qid +
                ", joinTime=" + joinTime +
                "}";
    }
}
