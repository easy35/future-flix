package com.flix.futureflix.model.circle;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class QuanziVo {
    @ApiModelProperty(value = "圈子用户关系id",required = true)
    private Integer uqId;

    @ApiModelProperty(value = "圈子id",required = true)
    private Integer qid;
    private String qname;
    private String imgUrl;
}
