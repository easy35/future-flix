package com.flix.futureflix.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;


public class Dashang implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "dashang_id", type = IdType.AUTO)
    private Integer dashangId;

    @ApiModelProperty(value = "积分",required = true)
    private Integer jifen;

    @ApiModelProperty(value = "用户id",required = true)
    private Integer uid;

    @ApiModelProperty(value = "帖子id",required = true)
    private Integer tid;


    public Integer getDashangId() {
        return dashangId;
    }

    public void setDashangId(Integer dashangId) {
        this.dashangId = dashangId;
    }

    public Integer getJifen() {
        return jifen;
    }

    public void setJifen(Integer jifen) {
        this.jifen = jifen;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public Integer getTid() {
        return tid;
    }

    public void setTid(Integer tid) {
        this.tid = tid;
    }

    @Override
    public String toString() {
        return "Dashang{" +
                "dashangId=" + dashangId +
                ", jifen=" + jifen +
                ", uid=" + uid +
                ", tid=" + tid +
                "}";
    }
}
