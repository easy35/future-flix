package com.flix.futureflix.model.tiezi;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author h
 * @since 2024-04-22
 */
public class Tiezi implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "tid", type = IdType.AUTO)
    private Integer tid;

    private Integer qid;

    private Integer uid;


    private Integer tieziState;

    private String tieziContent;

    private LocalDateTime tieziTime;

    private Integer approval;

    private Integer objection;

    private Integer pinglunNum;

    private String title;

    private Integer totalScore;


    public Integer getTid() {
        return tid;
    }

    public void setTid(Integer tid) {
        this.tid = tid;
    }

    public Integer getQid() {
        return qid;
    }

    public void setQid(Integer qid) {
        this.qid = qid;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }


    public Integer getTieziState() {
        return tieziState;
    }

    public void setTieziState(Integer tieziState) {
        this.tieziState = tieziState;
    }

    public String getTieziContent() {
        return tieziContent;
    }

    public void setTieziContent(String tieziContent) {
        this.tieziContent = tieziContent;
    }

    public LocalDateTime getTieziTime() {
        return tieziTime;
    }

    public void setTieziTime(LocalDateTime tieziTime) {
        this.tieziTime = tieziTime;
    }

    public Integer getApproval() {
        return approval;
    }

    public void setApproval(Integer approval) {
        this.approval = approval;
    }

    public Integer getObjection() {
        return objection;
    }

    public void setObjection(Integer objection) {
        this.objection = objection;
    }

    public Integer getPinglunNum() {
        return pinglunNum;
    }

    public void setPinglunNum(Integer pinglunNum) {
        this.pinglunNum = pinglunNum;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(Integer totalScore) {
        this.totalScore = totalScore;
    }

    @Override
    public String toString() {
        return "Tiezi{" +
                "tid=" + tid +
                ", qid=" + qid +
                ", uid=" + uid +
                ", tieziState=" + tieziState +
                ", tieziContent=" + tieziContent +
                ", tieziTime=" + tieziTime +
                ", approval=" + approval +
                ", objection=" + objection +
                ", pinglunNum=" + pinglunNum +
                ", title=" + title +
                ", totalScore=" + totalScore +
                "}";
    }
}
