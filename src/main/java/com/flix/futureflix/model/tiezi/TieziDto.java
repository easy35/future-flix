package com.flix.futureflix.model.tiezi;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel( description= "TieziDto帖子前后端交互实体类")
public class TieziDto {
    @ApiModelProperty(value = "帖子id" )
    private Integer tid;
    @ApiModelProperty(value = "圈子id")
    private Integer qid;
    @ApiModelProperty(value = "用户id",required = true)
    private Integer uid;
    @ApiModelProperty("帖子状态,默认为发布帖子 ")
    private Integer tieziState;
    @ApiModelProperty(value = "标题,长度不能超过30",required = true)
    private String title;
    @ApiModelProperty(value = "帖子内容",required = true)
    private String tieziContent;


}