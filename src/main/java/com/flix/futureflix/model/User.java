package com.flix.futureflix.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author h
 * @since 2024-04-19
 */
@Data
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "uid", type = IdType.AUTO)
    private Integer uid;

    private String uname;

    private String upwd;

    private Integer score;
    @TableField(value = "userImg")
    private String userImg;

//    @Override
//    public String toString() {
//        return "User{" +
//        "uid=" + uid +
//        ", uname=" + uname +
//        ", upwd=" + upwd +
//        ", score=" + score +
//        "}";
//    }
}
