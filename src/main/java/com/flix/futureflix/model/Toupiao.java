package com.flix.futureflix.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author h
 * @since 2024-04-26
 */
public class Toupiao implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "toupiao_id", type = IdType.AUTO)
    private Integer toupiaoId;
    @ApiModelProperty(value = "选择（0赞成，1反对）",required = true)
    private Integer choice;
    @ApiModelProperty(value = "用户id",required = true)
    private Integer uid;
    @ApiModelProperty(value = "帖子id",required = true)
    private Integer tid;


    public Integer getToupiaoId() {
        return toupiaoId;
    }

    public void setToupiaoId(Integer toupiaoId) {
        this.toupiaoId = toupiaoId;
    }

    public Integer getChoice() {
        return choice;
    }

    public void setChoice(Integer choice) {
        this.choice = choice;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public Integer getTid() {
        return tid;
    }

    public void setTid(Integer tid) {
        this.tid = tid;
    }

    @Override
    public String toString() {
        return "Toupiao{" +
                "toupiaoId=" + toupiaoId +
                ", choice=" + choice +
                ", uid=" + uid +
                ", tid=" + tid +
                "}";
    }
}
