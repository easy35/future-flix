package com.flix.futureflix.model.common;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Data
@Slf4j
public class PageRequestDto {
    @ApiModelProperty("每页容量")
    protected Integer size;
    @ApiModelProperty("页码")
    protected Integer page;

    public void checkParam() {
        if (this.page == null || this.page < 0) {
            setPage(0);
        }
        if (this.size == null || this.size <= 0 || this.size > 100) {
            setSize(5);
        }
    }
}
