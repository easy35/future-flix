package com.flix.futureflix.model.tiezi;

import com.flix.futureflix.model.common.PageRequestDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel(description = "帖子分页和条件查询参数类")
@Data
public class TieziPageDto extends PageRequestDto {
    @ApiModelProperty("用户id")
    private Integer uid;
    @ApiModelProperty("圈子id")
    private Integer qid;
    @ApiModelProperty("用户名")
    private String uname;
    @ApiModelProperty("帖子状态: 0 草稿 1待审核 2审核失败 3审核成功 4精品帖子 ")
    private Integer tieziState;
    @ApiModelProperty("标题")
    private String title;
    //limit起始个数
    private int offset ;
}
