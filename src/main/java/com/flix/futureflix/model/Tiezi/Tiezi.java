package com.flix.futureflix.model.Tiezi;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.time.LocalDateTime;

@ApiModel("帖子")
public class Tiezi implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "tid", type = IdType.AUTO)
@ApiModelProperty("帖子id")
    private Integer tid;
    @ApiModelProperty("圈子id")

    private Integer qid;
    @ApiModelProperty("用户id")

    private Integer uid;

    private Integer mid;
    /**
     * 0 草稿 1待审核 2审核成功 3审核失败
     */
    @ApiModelProperty("帖子状态: 0 草稿 1待审核 2审核成功 3审核失败 4精品帖子")

    private Integer tieziState;
    @ApiModelProperty("帖子内容")

    private String tieziContent;
    @ApiModelProperty("发帖时间")

    private LocalDateTime tieziTime;
    @ApiModelProperty("点赞数")

    private Integer approval;
    @ApiModelProperty("批评数")

    private Integer objection;
    @ApiModelProperty("评论数")

    private Integer pinglunNum;
    @ApiModelProperty("标题")

    private String title;
    @ApiModelProperty("评分")

    private Integer totalScore;


    public Integer getTid() {
        return tid;
    }

    public void setTid(Integer tid) {
        this.tid = tid;
    }

    public Integer getQid() {
        return qid;
    }

    public void setQid(Integer qid) {
        this.qid = qid;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public Integer getMid() {
        return mid;
    }

    public void setMid(Integer mid) {
        this.mid = mid;
    }

    public Integer getTieziState() {
        return tieziState;
    }

    public void setTieziState(Integer tieziState) {
        this.tieziState = tieziState;
    }

    public String getTieziContent() {
        return tieziContent;
    }

    public void setTieziContent(String tieziContent) {
        this.tieziContent = tieziContent;
    }

    public LocalDateTime getTieziTime() {
        return tieziTime;
    }

    public void setTieziTime(LocalDateTime tieziTime) {
        this.tieziTime = tieziTime;
    }

    public Integer getApproval() {
        return approval;
    }

    public void setApproval(Integer approval) {
        this.approval = approval;
    }

    public Integer getObjection() {
        return objection;
    }

    public void setObjection(Integer objection) {
        this.objection = objection;
    }

    public Integer getPinglunNum() {
        return pinglunNum;
    }

    public void setPinglunNum(Integer pinglunNum) {
        this.pinglunNum = pinglunNum;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(Integer totalScore) {
        this.totalScore = totalScore;
    }

    @Override
    public String toString() {
        return "Tiezi{" +
        "tid=" + tid +
        ", qid=" + qid +
        ", uid=" + uid +
        ", mid=" + mid +
        ", tieziState=" + tieziState +
        ", tieziContent=" + tieziContent +
        ", tieziTime=" + tieziTime +
        ", approval=" + approval +
        ", objection=" + objection +
        ", pinglunNum=" + pinglunNum +
        ", title=" + title +
        ", totalScore=" + totalScore +
        "}";
    }
}
