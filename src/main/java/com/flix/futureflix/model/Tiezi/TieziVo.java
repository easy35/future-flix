package com.flix.futureflix.model.tiezi;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class TieziVo {
    @ApiModelProperty("用户id")
    private Integer uid;

    @ApiModelProperty("帖子id")
    private Integer tid;

    @ApiModelProperty("圈子id")
    private Integer qid;

    @ApiModelProperty("用户名")
    private String uname;

    @ApiModelProperty("用户头像地址")
    private String userImg;
    
    @ApiModelProperty("帖子状态: 0 草稿 1待审核 2审核成功 3审核失败 4精品帖子")
    private Integer tieziState;

    @ApiModelProperty("帖子内容")
    private String tieziContent;

    @ApiModelProperty("发帖时间")
    private LocalDateTime tieziTime;

    @ApiModelProperty("点赞数")
    private Integer approval;

    @ApiModelProperty("批评数")
    private Integer objection;

    @ApiModelProperty("评论数")
    private Integer pinglunNum;

    @ApiModelProperty("标题")
    private String title;

    @ApiModelProperty("评分")
    private Integer totalScore;

}
