package com.flix.futureflix.model.shensu;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.util.List;

@Data
public class ShensuListForUser {
    private List<Shensu> list;

    private String uname;

    private Integer score;
    @TableField(value = "userImg")
    private String userImg;
}
