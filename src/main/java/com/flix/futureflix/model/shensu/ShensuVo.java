package com.flix.futureflix.model.shensu;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.time.LocalDateTime;
@Data
public class ShensuVo {
    private Integer sid;

    private Integer tid;

    private Integer uid;

    private String reason;

    private Integer result;

    private LocalDateTime shensuTime;

    private Integer scoring;

    private String uname;
    private String userImg;

}
