package com.flix.futureflix.model.shensu;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;
import java.time.LocalDateTime;

public class Shensu implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "sid", type = IdType.AUTO)
    private Integer sid;

    private Integer tid;

    private Integer uid;


    private String reason;

    private Integer shensuState;

    private Integer result;

    private LocalDateTime shensuTime;

    private Integer scoring;


    public Integer getSid() {
        return sid;
    }

    public void setSid(Integer sid) {
        this.sid = sid;
    }

    public Integer getTid() {
        return tid;
    }

    public void setTid(Integer tid) {
        this.tid = tid;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Integer getShensuState() {
        return shensuState;
    }

    public void setShensuState(Integer shensuState) {
        this.shensuState = shensuState;
    }

    public Integer getResult() {
        return result;
    }

    public void setResult(Integer result) {
        this.result = result;
    }

    public LocalDateTime getShensuTime() {
        return shensuTime;
    }

    public void setShensuTime(LocalDateTime shensuTime) {
        this.shensuTime = shensuTime;
    }

    public Integer getScoring() {
        return scoring;
    }

    public void setScoring(Integer scoring) {
        this.scoring = scoring;
    }

    @Override
    public String toString() {
        return "Shensu{" +
        "sid=" + sid +
        ", tid=" + tid +
        ", uid=" + uid +

        ", reason=" + reason +
        ", shensuState=" + shensuState +
        ", result=" + result +
        ", shensuTime=" + shensuTime +
        ", scoring=" + scoring +
        "}";
    }
}
