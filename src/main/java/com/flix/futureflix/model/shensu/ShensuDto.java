package com.flix.futureflix.model.shensu;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class ShensuDto {
    @ApiModelProperty(required = true)
    private Integer sid;

    @ApiModelProperty(required = true,name = "申述理由")
    private Integer result;



}
