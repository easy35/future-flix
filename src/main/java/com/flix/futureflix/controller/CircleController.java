package com.flix.futureflix.controller;

import com.flix.futureflix.model.circle.Circle;
import com.flix.futureflix.model.circle.UserQuanzi;
import com.flix.futureflix.model.common.ResponseResult;
import com.flix.futureflix.service.CircleService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/circles")
public class CircleController {
    @Autowired
    private CircleService circleService;

    @ApiOperation("获取所有圈子")
    @PostMapping("/all")
    public ResponseResult<List<Circle>> getAllCircles(){
        return ResponseResult.okResult(circleService.getAllCircles());
    }
    @ApiOperation("获取用户圈子")
    @PostMapping("/user/all")
    public ResponseResult getUserCircles(@RequestBody Integer uid){
        return circleService.getUserCircle(uid);
    }

    @ApiOperation("加入圈子/重新加入圈子")
    @PostMapping("/join")
    public ResponseResult joinCircle(@RequestBody UserQuanzi dto){
        return circleService.joinCircle(dto);
    }


    @ApiOperation("退出圈子")
    @DeleteMapping("/exit/{uqid}")
    public ResponseResult exitCircle(@PathVariable Integer uqid){
        return circleService.exitCircle(uqid);
    }
}
