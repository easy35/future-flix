package com.flix.futureflix.controller;

import com.flix.futureflix.model.User;
import com.flix.futureflix.model.common.ResponseResult;
import com.flix.futureflix.service.UserService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(description = "用户管理")
@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;
    @GetMapping("{id}")
    public ResponseResult<User> getUser(@PathVariable Integer id){
        return userService.getUser(id);
    }
}
