package com.flix.futureflix.controller;

import com.flix.futureflix.model.shensu.Shensu;
import com.flix.futureflix.model.common.ResponseResult;
import com.flix.futureflix.model.shensu.ShensuDto;
import com.flix.futureflix.model.shensu.ShensuListForUser;
import com.flix.futureflix.service.ShensuService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Api("申诉管理API")
@RestController
@RequestMapping("/shensu")
public class ShensuController {
    // 自动注入
    @Autowired
    private ShensuService shensuService;
    @ApiOperation("提交申诉")
    @ApiResponse(code = 200, message = "操作成功")
    @PostMapping("/submit")
    public ResponseResult submitShensu(@RequestParam("tid") Integer tid,
                                       @RequestParam("uid") Integer uid,
                                       @RequestParam("reason") String reason) {
        // 提交申诉
        return shensuService.submitShensu(tid, uid, reason);
    }


    @ApiOperation("获取申诉列表")
    @ApiResponse(code = 200, message = "操作成功")
    @GetMapping("/list")
    public ResponseResult getShensuList() {
        // 获取申诉列表
        return shensuService.getShensuList();
    }

    @ApiOperation("获取用户申诉列表")
    @ApiResponse(code = 200, message = "操作成功")
    @GetMapping("/list/user/{id}")
    public ResponseResult<ShensuListForUser> getShensuList(@PathVariable Integer id) {
        // 获取申诉列表
        return shensuService.getShensuByUser(id);
    }


    @ApiOperation("处理申诉")
    @ApiResponse(code = 200, message = "操作成功")
    @PostMapping("/handle")

    public ResponseResult<Shensu> handleShensu(@RequestBody ShensuDto dto) {
        // 处理申诉
        return shensuService.handleShensu(dto);
    }
    @ApiOperation("给服务打分")
    @ApiResponse(code = 200, message = "操作成功")
    @PostMapping("/rate")
    public ResponseResult rateService(@RequestParam("uid") Integer uid,
                                      @RequestParam("sid") Integer sid,
                                      @RequestParam("score") Integer score) {
        // 给服务打分
        return shensuService.rateService(uid, sid, score);
    }

    @ApiOperation("下架帖子并通知用户")
    @ApiResponse(code = 200, message = "帖子已成功下架，并已通知用户。")
    @PostMapping("/deleteAndNotify")
    public ResponseResult deleteAndNotify(@RequestParam("tid") Integer tid) {
        // 下架帖子并通知用户
        return shensuService.deleteAndNotify(tid);
    }
    @ApiOperation("选精品帖子")
    @ApiResponse(code = 200, message = "操作成功")
    @PostMapping("/selectGoodTiezi")
    public ResponseResult selectGoodTiezi(@RequestParam("tid") Integer tid) {
        // 选精品帖子
        return shensuService.selectGoodTiezi(tid);
    }
    @ApiOperation("查询精品帖子列表")
    @ApiResponse(code = 200, message = "操作成功")
    @GetMapping("/goodTieziList")
    public ResponseResult getGoodTieziList() {
        // 查询精品帖子列表
        return ResponseResult.okResult(shensuService.getGoodTieziList());
    }

}
