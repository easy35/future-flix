package com.flix.futureflix.controller;

import com.flix.futureflix.common.AppHttpCodeEnum;
import com.flix.futureflix.model.Dashang;
import com.flix.futureflix.model.Toupiao;
import com.flix.futureflix.model.common.ResponseResult;
import com.flix.futureflix.model.pinglun.likeDto;
import com.flix.futureflix.model.pinglun.pinglun;
import com.flix.futureflix.model.pinglun.pinglunDto;
import com.flix.futureflix.model.pinglun.pinglunVo;
import com.flix.futureflix.model.tiezi.TieziDto;
import com.flix.futureflix.model.tiezi.TieziPageDto;
import com.flix.futureflix.model.tiezi.TieziVo;
import com.flix.futureflix.service.TieziService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Api(description = "帖子内容管理")
@RestController
@RequestMapping("/tiezi")
public class TieziController {
    @Autowired
    private TieziService tieziService;
    @ApiOperation("上传图片,返回图片url")
    @PostMapping("/upload")
    public ResponseResult<String> upload(@RequestPart("wangeditor-uploaded-image") MultipartFile multipartFile){
        if (multipartFile.getContentType() != null) {
            return tieziService.upload(multipartFile);
        } else {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID,"请上传图片");
        }
    }

    @ApiOperation("提交帖子")
    @ApiResponses({
        @ApiResponse(code = 501,message="无效参数"),
        @ApiResponse(code = 201,message="操作失败")
    })
    @PostMapping("/submit")
    public ResponseResult submitTiezi(@ApiParam(value = "参数详情请看TieziDto") @RequestBody TieziDto dto){
        return tieziService.submitTiezi(dto);
    }

    @ApiOperation("分页带条件搜索")
    @PostMapping("search")
    public ResponseResult<List<TieziVo>> search(@ApiParam(value = "参数详情请看TieziPageDto") @RequestBody TieziPageDto dto){
        return tieziService.getTieziList(dto);
    }

    @ApiOperation("删除帖子")
    @ApiResponse(code = 501,message="无效参数")
    @DeleteMapping("/del/{id}")
    public ResponseResult del(@ApiParam(value="帖子id") @PathVariable Integer id){
        return tieziService.delTiezi(id);
    }

    @ApiOperation("查看帖子详情")
    @ApiResponse(code = 501,message="无效参数")
    @GetMapping("/detail/{id}")
    public ResponseResult<TieziVo> getDetail(@PathVariable Integer id){
        return tieziService.getTieziDetail(id);
    }

    @ApiOperation("投票(每人每个帖子只能投一票，0赞成，1反对)")
    @PostMapping("/detail/vote")
    public ResponseResult voteForPost(@RequestBody Toupiao dto){
        int keys=tieziService.voteForPost(dto);
        if(keys==-2){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID,"找不到帖子或者没有选择");
        }else if(keys==-1){
            return ResponseResult.errorResult(AppHttpCodeEnum.FAILED,"每人每天只能投一张票");
        }else{
            return ResponseResult.okResult(keys);
        }
    }

    @ApiOperation("打赏")
    @PostMapping("/detail/reward")
    public ResponseResult reward(@RequestBody Dashang dto){
        return tieziService.reward(dto);
    }

    @ApiOperation("添加评论")
    @PostMapping("/detail/addcomment")
    public ResponseResult addcomment(@RequestBody pinglun dto){
        return tieziService.addcomment(dto);
    }

    @ApiOperation("删除评论")
    @PostMapping("/detail/delcomment")
    public ResponseResult delcomment(@RequestBody Integer pid){
        return tieziService.delcomment(pid);
    }

    @ApiOperation("点赞评论(True)/取消点赞(False)")
    @PostMapping("/detail/likecomment")
    public ResponseResult likecomment(@RequestBody likeDto dto){
        return tieziService.likecomment(dto);
    }

    @ApiOperation("获取该帖子所有评论(同时传送该用户对评论的点赞状态)")
    @PostMapping("/detail/allcomments")
    public ResponseResult<List<pinglunVo>> getAllComments(@RequestBody pinglunDto dto){
        return tieziService.getAllComment(dto);

    }


}
