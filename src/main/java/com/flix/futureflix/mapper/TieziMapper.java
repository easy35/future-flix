package com.flix.futureflix.mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.flix.futureflix.model.tiezi.Tiezi;
import com.flix.futureflix.model.tiezi.TieziPageDto;
import com.flix.futureflix.model.tiezi.TieziVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TieziMapper extends BaseMapper<Tiezi> {

    List<TieziVo> selectTieziVoPage(@Param("query") TieziPageDto dto);
    int getTieziCount(@Param("query") TieziPageDto dto);
}
