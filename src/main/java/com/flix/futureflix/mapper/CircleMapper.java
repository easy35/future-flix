package com.flix.futureflix.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.flix.futureflix.model.circle.Circle;
import com.flix.futureflix.model.circle.QuanziVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface CircleMapper extends BaseMapper<Circle> {
    public List<QuanziVo> selectQuanziVoByUser(@Param("uid") Integer uid);

}

