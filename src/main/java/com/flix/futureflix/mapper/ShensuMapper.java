package com.flix.futureflix.mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.flix.futureflix.model.shensu.Shensu;
import com.flix.futureflix.model.shensu.ShensuVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ShensuMapper extends BaseMapper<Shensu> {
    public List<ShensuVo> selectShensuVo();
    public List<ShensuVo> selectShensuVoByUser(Integer uid);
}
