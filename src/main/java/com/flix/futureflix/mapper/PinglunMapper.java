package com.flix.futureflix.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.flix.futureflix.model.pinglun.pinglun;
import com.flix.futureflix.model.pinglun.pinglunDto;
import com.flix.futureflix.model.pinglun.pinglunVo;
import com.flix.futureflix.model.tiezi.TieziVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface PinglunMapper extends BaseMapper<pinglun> {
    List<pinglunVo> selectPinglunVo(@Param("dto") pinglunDto dto);

}
