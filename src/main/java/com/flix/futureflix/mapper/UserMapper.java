package com.flix.futureflix.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.flix.futureflix.model.User;

public interface UserMapper extends BaseMapper<User> {
}
