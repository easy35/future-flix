package com.flix.futureflix;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.flix.futureflix.mapper")
public class FutureFlixApplication {

    public static void main(String[] args) {
        SpringApplication.run(FutureFlixApplication.class, args);
    }

}
