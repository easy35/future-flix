package com.flix.futureflix.service;


import com.flix.futureflix.model.Dashang;
import com.flix.futureflix.model.Toupiao;
import com.flix.futureflix.model.common.ResponseResult;
import com.flix.futureflix.model.pinglun.likeDto;
import com.flix.futureflix.model.pinglun.pinglun;
import com.flix.futureflix.model.pinglun.pinglunDto;
import com.flix.futureflix.model.tiezi.Tiezi;
import com.flix.futureflix.model.tiezi.TieziDto;
import com.flix.futureflix.model.tiezi.TieziPageDto;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * 帖子内容管理类
 */
public interface TieziService {
    /**
     * 上传图片
     * @param photo 图片
     * @return
     */
    ResponseResult upload(@RequestParam MultipartFile photo);

    /**
     * 上传帖子
     * @return
     */
    ResponseResult submitTiezi(@RequestParam TieziDto dto);

    /**
     * 删除帖子
     * @param id
     * @return
     */
    ResponseResult delTiezi(Integer id);

    /**
     * 查询帖子
     * @param dto
     * @return
     */
    ResponseResult getTieziList( TieziPageDto dto);

    /**
     * 获取帖子详情
     * @param id
     * @return
     */
    ResponseResult getTieziDetail(Integer id);

//    /**
//     * 设置状态
//     * @param tid
//     * @return
//     */
//    ResponseResult setStatus(Integer tid);

    List<Tiezi> getCTieziById(int id);

    int voteForPost(Toupiao dto);

    ResponseResult reward(Dashang dto);

    ResponseResult addcomment(pinglun dto);

    ResponseResult delcomment(Integer pid);

    ResponseResult likecomment(likeDto dto);

    ResponseResult getAllComment(pinglunDto dto);

}
