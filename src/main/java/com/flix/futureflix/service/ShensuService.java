package com.flix.futureflix.service;


import com.flix.futureflix.model.shensu.Shensu;
import com.flix.futureflix.model.common.ResponseResult;
import com.flix.futureflix.model.shensu.ShensuDto;
import com.flix.futureflix.model.tiezi.Tiezi;

import java.util.List;

public interface ShensuService {
    ResponseResult submitShensu(Integer tid, Integer uid, String reason);// 提交申诉

    ResponseResult getShensuList(); // 查询申诉列表

    ResponseResult handleShensu(ShensuDto dto); // 处理申诉

    ResponseResult deleteAndNotify(Integer tid);//删除帖子
    ResponseResult rateService(Integer uid, Integer sid, Integer score);//打分
    ResponseResult selectGoodTiezi(Integer tid);// 选精品帖子
    List<Tiezi> getGoodTieziList();

    ResponseResult getShensuByUser(Integer id);
}
