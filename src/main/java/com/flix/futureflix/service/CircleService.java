package com.flix.futureflix.service;

import com.flix.futureflix.model.circle.Circle;
import com.flix.futureflix.model.circle.UserQuanzi;
import com.flix.futureflix.model.common.ResponseResult;

import java.util.List;

public interface CircleService {
    List<Circle> exposeCircle();

    ResponseResult joinCircle(UserQuanzi dto);

    ResponseResult getUserCircle(Integer uid);

    Object getAllCircles();

    ResponseResult exitCircle(Integer uqid);
}
