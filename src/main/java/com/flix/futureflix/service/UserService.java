package com.flix.futureflix.service;

import com.flix.futureflix.model.common.ResponseResult;

public interface UserService {
    public ResponseResult getUser(Integer id);
}
