package com.flix.futureflix.service.impl;

import com.flix.futureflix.common.AppHttpCodeEnum;
import com.flix.futureflix.mapper.UserMapper;
import com.flix.futureflix.model.User;
import com.flix.futureflix.model.common.ResponseResult;
import com.flix.futureflix.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;
    @Override
    public ResponseResult getUser(Integer id) {
        if(id==null){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }
        User user=userMapper.selectById(id);
        if(user==null){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        user.setUpwd("");
        ResponseResult<User> result=new ResponseResult<>();
        return result.ok(user);
    }
}
