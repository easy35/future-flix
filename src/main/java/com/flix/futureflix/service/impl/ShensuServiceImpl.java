package com.flix.futureflix.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.flix.futureflix.mapper.TieziMapper;
import com.flix.futureflix.mapper.UserMapper;
import com.flix.futureflix.model.User;
import com.flix.futureflix.model.shensu.Shensu;
import com.flix.futureflix.common.AppHttpCodeEnum;
import com.flix.futureflix.mapper.ShensuMapper;
import com.flix.futureflix.model.shensu.ShensuDto;
import com.flix.futureflix.model.shensu.ShensuListForUser;
import com.flix.futureflix.model.tiezi.Tiezi;
import com.flix.futureflix.service.ShensuService;
import com.flix.futureflix.model.common.ResponseResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;


import com.flix.futureflix.common.TieziConstant;
@Service
public class ShensuServiceImpl extends ServiceImpl<ShensuMapper, Shensu> implements ShensuService {
@Autowired
private UserMapper userMapper;
    @Autowired
    private ShensuMapper shensuMapper;
    @Override
    public ResponseResult submitShensu(Integer tid, Integer uid, String reason) {
        // 提交申诉
        // 查询帖子的申诉状态
        List<Shensu> shensuList = shensuMapper.selectList(new QueryWrapper<Shensu>()
                .eq("tid", tid)
                .eq("uid", uid));

        // 判断申诉状态是否已达到上限
        if (shensuList.size() >= 2) {
            // 返回自定义错误提示
            return ResponseResult.errorResult(400, "您已达到二次申诉上限，不能再申诉了。");
        }

        // 如果申诉状态没有达到上限，继续处理申诉逻辑
        // 设置申诉状态
        int shensuState = shensuList.size() + 1; // 申诉次数为当前申诉列表的大小加一
        // 提交新的申诉
        Shensu newShensu = new Shensu();
        newShensu.setTid(tid);
        newShensu.setUid(uid);
        newShensu.setReason(reason);
        newShensu.setShensuTime(LocalDateTime.now()); // 设置申诉时间为当前时间
        newShensu.setShensuState(shensuState); // 设置申诉状态
        newShensu.setResult(-1); // 设置申诉结果为默认值
        newShensu.setScoring(-1); // 设置默认打分为0
        int result = shensuMapper.insert(newShensu); // 保存申诉信息到数据库
        if (result > 0) {
            return ResponseResult.okResult(newShensu);
        } else {
            return ResponseResult.errorResult(500, "申诉提交失败，请稍后再试。");
        }
    }

    @Override
    public ResponseResult getShensuList() {
        // 查询申诉列表
        return ResponseResult.okResult( shensuMapper.selectShensuVo());
    }

    @Autowired
    private TieziMapper tieziMapper;

    @Override
    public ResponseResult handleShensu(ShensuDto dto) {
        if(dto==null||dto.getResult()==null||dto.getSid()==null){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }
        // 处理申诉
        Shensu shensu = shensuMapper.selectById(dto.getSid());
        if (shensu != null) {
            shensu.setResult(dto.getResult());

            shensuMapper.updateById(shensu);

            Tiezi tiezi = tieziMapper.selectById(shensu.getTid()); // 获取帖子对象
            if (tiezi != null && shensu.getResult() == 1 && tiezi.getTieziState() == TieziConstant.TIEZI_STATE_FAILURE) {
                // 如果申诉结果为1且帖子状态为TIEZI_STATE_CHECKING，恢复帖子状态为TIEZI_STATE_FAILURE
                tiezi.setTieziState(TieziConstant.TIEZI_STATE_SUCCESS);
                tieziMapper.updateById(tiezi);
            }

            return ResponseResult.okResult(shensu); // 使用okResult方法
        } else {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID, "申诉不存在"); // 使用errorResult方法
        }
    }
    @Override
    public ResponseResult deleteAndNotify(Integer tid) {
        // 下架帖子并通知用户
        Tiezi tiezi = tieziMapper.selectById(tid);
        if (tiezi != null) {
            tiezi.setTieziState(TieziConstant.TIEZI_STATE_FAILURE); // 设置帖子状态为下架
            tieziMapper.updateById(tiezi);
            // 通知用户的逻辑可以在此处实现，比如发送邮件或者消息
            // notifyUserAboutOffline(tiezi);
            return ResponseResult.okResult(200, "帖子已成功下架，并已通知用户。");
        } else {
            return ResponseResult.errorResult(500, "帖子不存在");
        }
    }
    @Override
    public ResponseResult rateService(Integer uid, Integer sid, Integer score) {
        // 打分
        Shensu shensu = shensuMapper.selectById(sid);
        if (shensu != null) {
            shensu.setScoring(score);
            shensuMapper.updateById(shensu);
            return ResponseResult.okResult(shensu);
        } else {
            return ResponseResult.errorResult(AppHttpCodeEnum.SUCCESS, "申诉不存在");
        }
    }
    @Override
    public ResponseResult selectGoodTiezi(Integer tid) {
        // 选精品帖子
        Tiezi tiezi = tieziMapper.selectById(tid);
        if (tiezi != null && tiezi.getApproval() > 30) {
            tiezi.setTieziState(TieziConstant.TIEZI_STATE_FINE);//精品
            tieziMapper.updateById(tiezi);
            return ResponseResult.okResult(200, "帖子成功标记为精品。");
        } else {
            return ResponseResult.errorResult(500, "帖子赞同数必须超过30才能标记为精品。");
        }
    }
    @Override
    public List<Tiezi> getGoodTieziList() {
        return tieziMapper.selectList(new QueryWrapper<Tiezi>().eq("tiezi_state", 4));
    }

    @Override
    public ResponseResult getShensuByUser(Integer id) {
        if(id==null){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }
        ShensuListForUser shensuList=new ShensuListForUser();
        User user =userMapper.selectById(id);
        if(user==null){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID,"不存在该用户");
        }
        shensuList.setUname(user.getUname());
        shensuList.setUserImg(user.getUserImg());
        shensuList.setList(shensuMapper.selectList(new QueryWrapper<Shensu>().eq("uid", id).orderByDesc("shensu_time")));
        ResponseResult result=new ResponseResult<>();
        return result.ok(shensuList);
    }
}
