package com.flix.futureflix.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.exceptions.MybatisPlusException;
import com.flix.futureflix.common.AppHttpCodeEnum;
import com.flix.futureflix.mapper.CircleMapper;
import com.flix.futureflix.mapper.UserMapper;
import com.flix.futureflix.mapper.UserqMapper;
import com.flix.futureflix.model.circle.Circle;
import com.flix.futureflix.model.circle.QuanziVo;
import com.flix.futureflix.model.circle.UserQuanzi;
import com.flix.futureflix.model.common.ResponseResult;
import com.flix.futureflix.service.CircleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
@Slf4j
public class CircleServiceImpl implements CircleService {
    @Autowired
    private CircleMapper circleMapper;
    @Autowired
    private UserqMapper userqMapper;

    @Override
    public List<Circle> exposeCircle(){

        List<Circle> Circles=circleMapper.selectList(null);

        return Circles;
    }

    public ResponseResult getAllCircles(){

        return ResponseResult.okResult(circleMapper.selectList(null));
    }

    @Autowired
    private UserMapper userMapper;

    @Override
    public ResponseResult joinCircle(UserQuanzi dto){
        if (dto==null){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        if(userMapper.selectById(dto.getUid())==null){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID,"没有该用户");
        }
        UserQuanzi userQ=userqMapper.selectOne(new QueryWrapper<UserQuanzi>().eq("uid",dto.getUid()).eq("qid",dto.getQid()));
        if (userQ!=null){
            try{
                userqMapper.delete(new QueryWrapper<UserQuanzi>().eq("uid",dto.getUid()).eq("qid",dto.getQid()));
            }catch (Exception e){
                return ResponseResult.errorResult(AppHttpCodeEnum.FAILED,"数据库删除原有数据失败");
            }
        }
        dto.setJoinTime(LocalDateTime.now());
        try{
            int res=userqMapper.insert(dto);
            return ResponseResult.okResult(res);
        }catch (MybatisPlusException e){
            return ResponseResult.errorResult(AppHttpCodeEnum.FAILED,"数据库插入失败");
        }
    }

    @Override
    public ResponseResult exitCircle(Integer uqid) {
        UserQuanzi userQ=userqMapper.selectOne(new QueryWrapper<UserQuanzi>().eq("uq_id",uqid));
        if (userQ!=null){
            try{
                return ResponseResult.okResult(userqMapper.delete(new QueryWrapper<UserQuanzi>().eq("uq_id",uqid)));
            }catch (Exception e){
                return ResponseResult.errorResult(AppHttpCodeEnum.FAILED,"数据库删除原有数据失败");
            }
        }else{
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID,"该用户没有加入圈子");
        }


    }

    public ResponseResult getUserCircle(Integer uid){
        if(userMapper.selectById(uid)==null){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID,"没有该用户");
        }
        List<QuanziVo> list= circleMapper.selectQuanziVoByUser(uid);
        ResponseResult result=new ResponseResult<>();
        if(list!=null){
            return result.ok(list);
        }else{
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
    }

}
