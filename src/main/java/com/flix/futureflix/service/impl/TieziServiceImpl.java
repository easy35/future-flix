package com.flix.futureflix.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.flix.futureflix.common.AppHttpCodeEnum;
import com.flix.futureflix.common.TieziConstant;
import com.flix.futureflix.mapper.*;
import com.flix.futureflix.model.Dashang;
import com.flix.futureflix.model.Toupiao;
import com.flix.futureflix.model.pinglun.likeDto;
import com.flix.futureflix.model.pinglun.pinglun;
import com.flix.futureflix.model.pinglun.pinglunDto;
import com.flix.futureflix.model.pinglun.pinglunVo;
import com.flix.futureflix.model.shensu.Shensu;
import com.flix.futureflix.model.tiezi.*;

import com.flix.futureflix.model.User;
import com.flix.futureflix.model.common.PageResponseResult;
import com.flix.futureflix.model.common.ResponseResult;
import com.flix.futureflix.model.tiezi.TieziDto;
import com.flix.futureflix.model.tiezi.TieziPageDto;
import com.flix.futureflix.model.tiezi.TieziVo;
import com.flix.futureflix.service.TieziService;
import com.flix.futureflix.utils.AliyunOSSUtil;
import com.flix.futureflix.utils.baiduCloud.GreenTextScan;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

@Service
@Slf4j
public class TieziServiceImpl implements TieziService {
    @Autowired
    private PinglunMapper pinglunMapper;

    @Autowired
    private RedisTemplate<String,String> redisTemplate;


    @Autowired
    private TieziMapper tieziMapper;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private CircleMapper circleMapper;

    @Autowired
    private GreenTextScan greenTextScan;
    @Override
    public ResponseResult upload(MultipartFile photo) {
        if(photo==null|| photo.isEmpty()){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID,"图片为空");
        }

        String url=AliyunOSSUtil.OSSUploadFile(photo);
        return ResponseResult.okResult(url);
    }

    /**
     *
     * @param dto
     * 1.用户id不能为空,不能为负数,圈子id可以为空,圈子id不能为负数
     * 2.根据id检索用户不存在,圈子不存在
     * 3.int型输入了字符串,报错bad request 400
     * 4.TieziState只可以为null,[1-4]
     * 5.标题过长,不能超过20
     * 6.标题和内容不能为null
     * 7.标题和内容不能为空格,缩进,回车
     * 8.审核标题和内容不能包含暴力,色情,黄色,低俗辱骂词汇,广告(联系方式)
     * 9.如果不传入帖子id,则为新增文章,如果传入帖子id,则为更新号码为id的帖子,如果不存在,报错
     * @return
     */
    @Override
    public ResponseResult submitTiezi(TieziDto dto) {
        //1.0检查参数
        if(dto.getUid()==null||dto==null|| !StringUtils.isNotBlank(dto.getTieziContent())||!StringUtils.isNotBlank(dto.getTitle())){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }

        if(userMapper.selectById(dto.getUid())==null){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID,"不存在该用户");
        }
        if(dto.getQid()!=null&&circleMapper.selectById(dto.getQid())==null){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID,"不存在该圈子");
        }

        if(dto.getTitle().length()>=20){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID,"标题过长,不能超过20");
        }

        if(dto.getTieziState()==null){
            dto.setTieziState(TieziConstant.TIEZI_STATE_SUCCESS);
        }else if(dto.getTieziState()>TieziConstant.TIEZI_STATE_SUCCESS||dto.getTieziState()<0){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID,"帖子状态设置错误");
        }

        int flag=0;
        //2.0是草稿不需要审核,不是草稿就要进行审核了
        if(!dto.getTieziState().equals(TieziConstant.TIEZI_STATE_SUCCESS)){
            Map<String, String>result=greenTextScan.testScan(dto.getTitle()+dto.getTieziContent());
            if(!result.get("conclusionType").equals("1")){
                String msg=result.get("msg");
                return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID,msg);
            }else{
                dto.setTieziState(TieziConstant.TIEZI_STATE_SUCCESS);
            }
        }

        //3.0选择添加文章或者更新文章
        if(dto.getTid()==null){
            Tiezi tiezi = new Tiezi();
            BeanUtils.copyProperties(dto,tiezi);
            //初始化默认值
            tiezi.setTieziTime(LocalDateTime.now());
            tiezi.setObjection(0);
            tiezi.setPinglunNum(0);
            flag=tieziMapper.insert(tiezi);

        }//更新
        else{
            Tiezi tiezi = tieziMapper.selectById(dto.getTid());
            if(tiezi==null){
                return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID,"文章不存在");
            }
            BeanUtils.copyProperties(dto,tiezi);
            tiezi.setTieziTime(LocalDateTime.now());
            flag=tieziMapper.updateById(tiezi);
        }
        //4.0返回结果
        if(flag!=0){
            return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
        }else{
            return ResponseResult.errorResult(AppHttpCodeEnum.FAILED);
        }
    }


    /**
     *
     * @param dto
     * 1.用户id不能为负数,圈子id不能为负数,页数page不能为负数
     * 2.用户不存在,圈子不存在
     * 3.integer输入了字符串,报错bad request 400
     * 4.每页容量size取值范围0-100
     * 5.查询结果为0,无符合条件数据
     * 6.页码越界,无更多数据了
     * @return
     */
    @Override
    public ResponseResult getTieziList(TieziPageDto dto) {

        //1.0检查参数
        if(dto==null){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }
        else if(dto.getUid()!=null&&dto.getUid()<0){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE,"用户id不能为负数");
        }
        else if(dto.getQid()!=null&&dto.getQid()<0){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE,"圈子id不能为负数");
        }
        if(dto.getSize()==null ||dto.getPage()==null){
            dto.checkParam();
        }
        else if ( dto.getPage() < 0) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID,"页数page不能小于0,不能大于100");
        }
        else if ( dto.getSize() <= 0 || dto.getSize() > 100) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID,"每页容量size输入不合法");
        }
        //2.0获取总数
        int total= tieziMapper.getTieziCount(dto);
        if(total==0){
            return  ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST,"无符合条件数据");
        } else if (dto.getPage()*dto.getSize()>total) {
            return  ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST,"无更多数据了");
        }
        dto.setOffset(dto.getPage()* dto.getSize());
        //3.0查询
        List<TieziVo> result=tieziMapper.selectTieziVoPage( dto);
        //4.结果返回
        ResponseResult responseResult = new PageResponseResult(dto.getPage(),dto.getSize(),total);

        responseResult.setData(result);

        return responseResult;
    }

    @Autowired
    private ShensuMapper shensuMapper;
    @Override
    @Transactional  //事务
    public ResponseResult delTiezi(Integer id) {

        if(tieziMapper.selectById(id)==null){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        try{
            shensuMapper.delete(new LambdaQueryWrapper<Shensu>().eq(Shensu::getTid,id));
            pinglunMapper.delete(new LambdaQueryWrapper<pinglun>().eq(pinglun::getTid,id));
            tieziMapper.deleteById(id);
        }catch (Exception e){
            log.error(e.getMessage());
        }
        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);

    }

    @Override
    public ResponseResult getTieziDetail(Integer id) {
        Tiezi tiezi=tieziMapper.selectById(id);

        if(tiezi==null){
            return  ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID,"帖子不存在");
        }

        User user =new User();
        if(tiezi.getUid()!=null){
            user= userMapper.selectById(tiezi.getUid());
        }
        TieziVo vo=new TieziVo();
        BeanUtils.copyProperties(tiezi,vo);
        vo.setUname(user.getUname());
        vo.setUserImg(user.getUserImg());

        return ResponseResult.okResult(vo);
    }

    @Override
    public List<Tiezi> getCTieziById(int id){
        QueryWrapper<Tiezi> queryWrapper=new QueryWrapper<>();
        queryWrapper.eq("qid",id);
        return tieziMapper.selectList(queryWrapper);
    }

    @Override
    public int voteForPost(Toupiao dto) {
        if (dto.getTid() <= 0 || dto.getChoice() < 0) {
            return -2;
        }

        String cacheKey="user:"+dto.getUid()+":post:"+dto.getTid();

        if(redisTemplate.hasKey(cacheKey)){
            return -1;
        }

        Tiezi tiezi = tieziMapper.selectById(dto.getTid());

        if (tiezi == null) {
            return -2;
        }

        if (dto.getChoice() == 0) {
            tiezi.setApproval(tiezi.getApproval() + 1);
        } else {
            tiezi.setObjection(tiezi.getObjection() + 1);
        }

        tieziMapper.updateById(tiezi);

        redisTemplate.opsForValue().set(cacheKey,"voted",1, TimeUnit.DAYS);

        return (dto.getChoice() == 0) ? tiezi.getApproval() : tiezi.getObjection();

    }

    @Override
    public ResponseResult reward(Dashang dto){
        //参数检验
        if (dto.getTid()==null||dto.getJifen()==null||dto.getTid()<=0||dto.getJifen()<=0){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID,"参数无效");
        }

        //查询帖子
        Tiezi tiezi = tieziMapper.selectById(dto.getTid());
        if (tiezi==null){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID,"帖子不存在");
        }
        System.out.println(tiezi);

        //查询用户
        User user=userMapper.selectById(tiezi.getUid());
        if(user==null){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID,"用户不存在");
        }

        if (dto.getUid()==tiezi.getUid()){
            return ResponseResult.errorResult(AppHttpCodeEnum.FAILED,"不能给自己打赏");
        }

        //更新用户积分
        user.setScore(user.getScore()+dto.getJifen());
        userMapper.updateById(user);

        //更新帖子总积分
        tiezi.setTotalScore(tiezi.getTotalScore()+dto.getJifen());
        tieziMapper.updateById(tiezi);

        return ResponseResult.okResult("打赏成功");
    }

    @Transactional
    public ResponseResult addcomment(pinglun dto){
//        if (!isValidComment(dto)){
//            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID,"评论内容无效");
//        }
        //寻找帖子
        Tiezi tiezi=tieziMapper.selectById(dto.getTid());
        if (tiezi==null){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID,"帖子不存在");
        }

        //审核
        Map<String, String>result=greenTextScan.testScan(dto.getPinglunContent());
        if(!result.get("conclusionType").equals("1")){
            String msg=result.get("msg");
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID,msg);
        }

        try{
            //将评论插入数据库
            dto.setPinglunState(-1);
            pinglunMapper.insert(dto);

            //更改帖子状态（评论数+1）
            tiezi.setPinglunNum(tiezi.getPinglunNum()+1);
            tieziMapper.updateById(tiezi);

            return ResponseResult.okResult("添加评论成功");
        }catch (DataAccessException e){
            System.out.println(e);
            return ResponseResult.errorResult(AppHttpCodeEnum.FAILED,"数据库处理异常");
        }
    }
    @Transactional
    public ResponseResult delcomment(Integer pid){
        //检查评论是否存在
        pinglun ping=pinglunMapper.selectById(pid);
        if (ping==null){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID,"评论不存在");
        }
        try{
            //删除评论

            int flag=pinglunMapper.deleteById(pid);
            if (flag==0){
                return ResponseResult.errorResult(AppHttpCodeEnum.FAILED,"数据库中删除评论失败");
            }

            //删除与评论相关的redis键
            String pattern="*comment:"+pid;
            Set<String> keys=redisTemplate.keys(pattern);
            if(!CollectionUtils.isEmpty(keys)){
                redisTemplate.delete(keys);
            }

            //更新帖子评论数量
            Tiezi tiezi= tieziMapper.selectById(ping.getTid());
            if(tiezi!=null){
                tiezi.setPinglunNum(Math.max(tiezi.getPinglunNum()-1,0));//防止出现负数
                tieziMapper.updateById(tiezi);
            }

            return ResponseResult.okResult("删除评论成功");
        }catch(DataAccessException e){
            System.out.println(e);
            return ResponseResult.errorResult(AppHttpCodeEnum.SERVER_ERROR,"数据库操作异常");
        }catch (Exception e){
            System.out.println(e);
            return ResponseResult.errorResult(AppHttpCodeEnum.SERVER_ERROR,"服务器发生未知异常");
        }
    }

    @Transactional
    public ResponseResult likecomment(likeDto dto){
        //查找评论
        pinglun ping=pinglunMapper.selectById(dto.getPid());
        if (ping==null){
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST,"评论不存在");
        }
        String cacheKey="user:"+dto.getUid()+":comment:"+dto.getPid();
        boolean hasLiked=redisTemplate.hasKey(cacheKey);
        if(!hasLiked&&dto.isLiked()){
            ping.setDianzanNum(ping.getDianzanNum()+1);
            pinglunMapper.updateById(ping);
            redisTemplate.opsForValue().set(cacheKey,"liked");
            return ResponseResult.okResult(1);
        }
        else if(hasLiked&&!dto.isLiked()){
            ping.setDianzanNum(ping.getDianzanNum()-1);
            pinglunMapper.updateById(ping);
            redisTemplate.delete(cacheKey);
            return ResponseResult.okResult(0);
        }
        else{
            return ResponseResult.errorResult(AppHttpCodeEnum.FAILED,"前后端喜欢不喜欢不对应！");
        }
    }

    @Transactional
    public ResponseResult getAllComment(pinglunDto dto){
        //搜索该帖子的所有评论
        List<pinglunVo> comments=pinglunMapper.selectPinglunVo(dto);//此时所有评论状态皆为-1

        //通过搜索redis中的user:dto.getUid():comment:?来将点赞过的评论状态赋值
        for (pinglunVo comment:comments){
            String cacheKey="user:"+dto.getUid()+":comment:"+comment.getPid();
            if(redisTemplate.hasKey(cacheKey)){
                comment.setPinglunState(0);
            }
        }
        ResponseResult result=new ResponseResult<>();
        return  result.ok(comments);
    }

    /**
     * 更新状态
     * @param tid
     * @return
     */
//    @Override
//    public ResponseResult setStatus(Integer tid) {
//
//        Tiezi tiezi = tieziMapper.selectById(tid);
//        if(tiezi==null){
//            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID,"文章不存在");
//        }
//
//        if(tieziMapper.updateById(tiezi)==1){
//            return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
//        }else{
//            return ResponseResult.errorResult(AppHttpCodeEnum.FAILED);
//        }
//    }
}
