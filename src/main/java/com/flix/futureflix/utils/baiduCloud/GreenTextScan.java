package com.flix.futureflix.utils.baiduCloud;

import com.baidu.aip.contentcensor.AipContentCensor;
import lombok.Getter;
import lombok.Setter;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
@Component
@ConfigurationProperties(prefix = "baiducloud")
public class GreenTextScan {
    //设置APPID/AK/SK
    private String APP_ID;
    private String API_KEY;
    private String SECRET_KEY;

    /**
     *
     * @param content
     * @return
     * {
     *      "conclusionType" :1.合规，2.不合规，3.疑似，4.审核失败,
     *      "msg": "审核结果"
     * }
     */
    public Map<String, String> testScan(String content) {
        // 初始化一个AipContentCensor
        AipContentCensor client = new AipContentCensor(APP_ID, API_KEY, SECRET_KEY);
        Map<String, String> resultMap = new HashMap<>();
        JSONObject res = client.textCensorUserDefined(content);
        //返回的响应结果

        Integer conclusionType= res.getInt("conclusionType");
        resultMap.put("conclusionType", conclusionType.toString());
        //审核不成功
        if (conclusionType==1) {
            resultMap.put("msg","审核合规");
            return resultMap;
        } else if (conclusionType==4) {
            resultMap.put("msg","审核发起失败");
        }
//        获得特殊集合字段
        JSONArray dataArrays = res.getJSONArray("data");

        String msg = "";
        for (int i=0;i< dataArrays.length();i++) {
            JSONObject result = dataArrays.getJSONObject(i);
//            System.out.println(result);
            //获得原因
            msg += result.getString("msg")+'\n';
        }
        resultMap.put("msg", msg);
        return resultMap;
    }
}
