package com.flix.futureflix.common;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 0 草稿 1审核中 2审核失败 3审核成功 4精品帖子
 *
 */
@ApiModel("帖子状态")
public class TieziConstant {

    @ApiModelProperty("草稿")
    public static final Integer TIEZI_STATE_DRAFT=0;
    @ApiModelProperty("审核中")

    public static final Integer TIEZI_STATE_CHECKING=1;
    @ApiModelProperty("审核失败")


    public static final Integer TIEZI_STATE_FAILURE=2;
    @ApiModelProperty("审核成功,成功上线")

    public static final Integer TIEZI_STATE_SUCCESS=3;

    @ApiModelProperty("精品帖子")

    public static final Integer TIEZI_STATE_FINE=4;

}
