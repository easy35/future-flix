package com.flix.futureflix.common;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @version 1.0
 * @date 2021/5/29 21:45
 */
@Component
public class OssConstant implements InitializingBean {
    @Value("${aliyun.oss.file.endPoint}")
    private String oss_file_endpoint;

    @Value("${aliyun.oss.file.keyid}")
    private String oss_file_keyid;

    @Value("${aliyun.oss.file.keysecret}")
    private String oss_file_keysecret;

    @Value("${aliyun.oss.file.bucketname}")
    private String oss_file_bucketname;

    @Value("${aliyun.oss.file.address}")
    private String oss_file_address;

    public static String OSS_END_POINT;
    public static String OSS_BUCKET;
    public static String OSS_ACCESS_KEY_ID;
    public static String OSS_ACCESS_KEY_SECRET;

    public static String OSS_ADDRESS;


    @Override
    public void afterPropertiesSet() throws Exception {
        OSS_END_POINT = oss_file_endpoint;
        OSS_BUCKET = oss_file_bucketname;
        OSS_ACCESS_KEY_ID = oss_file_keyid;
        OSS_ACCESS_KEY_SECRET = oss_file_keysecret;
        OSS_ADDRESS=oss_file_address;
    }
}

