package com.flix.futureflix;

import com.flix.futureflix.mapper.TieziMapper;

import com.flix.futureflix.service.TieziService;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest(classes = FutureFlixApplication.class)
@RunWith(SpringRunner.class)public class test {
    @Autowired
    private TieziService tieziService;

    @Autowired
    private TieziMapper tieziMapper;

//    @Test
//    public void test(){
//        // 查询第一页，每页显示 10 条
//        Page<TieziVo> page = new Page<>(1, 10);
//        // 注意：一定要手动关闭 SQL 优化，不然查询总数的时候只会查询主表
//        page.setOptimizeCountSql(false);
//        // 组装查询条件 where age = 20
//        LambdaQueryWrapper<TieziVo> wrapper = new LambdaQueryWrapper<>();
////        queryWrapper.ge("tiezi_state", 0);
//
//        IPage<TieziVo> page1 = tieziMapper.getAllTieziList(page, wrapper);
//
//        System.out.println("总记录数：" + page1.getTotal());
//        System.out.println("总共多少页：" + page1.getPages());
//        System.out.println("当前页码：" + page1.getCurrent());
//        System.out.println("查询数据：" + page1.getRecords());
////        tieziMapper.getAllTieziList();
//    }
}

