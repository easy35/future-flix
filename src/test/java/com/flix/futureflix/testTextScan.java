package com.flix.futureflix;

import com.flix.futureflix.utils.baiduCloud.GreenTextScan;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
@Slf4j
@SpringBootTest(classes = FutureFlixApplication.class)
@RunWith(SpringRunner.class)
public class testTextScan {
    @Autowired
    private GreenTextScan greenTextScan;
    @Test
    public void test(){
        log.info( greenTextScan.testScan("你有没有").toString());
    }

}


