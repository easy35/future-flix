import { createApp } from "vue";
import "./assets/css/reset.less";

import App from "./App.vue";
import router from "./router/index";

import ElementPlus from "element-plus";
import "element-plus/dist/index.css";
//@ts-ignore
import zhCn from "element-plus/dist/locale/zh-cn.mjs";
// import "ant-design-vue/dist/reset.css";
import pinia from "./store";
import * as ElementPlusIconsVue from "@element-plus/icons-vue";
const app = createApp(App);
app.use(ElementPlus, {
  locale: zhCn, //element-plus国际化配置
});

app.use(pinia);

app.use(router);
//将element-plus提供图标注册为全局组件
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
  app.component(key, component);
}
app.mount("#app");
