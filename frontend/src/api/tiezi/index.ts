import request from "@/utils/request";

enum API {
  ALL_TIEZI = "/tiezi/search",
  SUBMIT_TIEZI = "/tiezi/submit",
  DETAIL_TIEZI = "/tiezi/detail",
  DEL_TIEZI = "/tiezi/del",
  TOUPIAO_URL = "/tiezi/detail/vote",
  DASHANG_URL = "/tiezi/detail/reward",
  ALLPINGLUN_URL = "/tiezi/detail/allcomments",
  POSTPINGLLUN_URL = "/tiezi/detail/addcomment",
  DELPINGLUN_URL = "/tiezi/detail/delcomment",
  DIANZANPINGLUN_URL = "/tiezi/detail/likecomment",
  SHENSU_URL = "/shensu/submit",
  ALLSHENSU_URL = "/shensu/list",
  HANDLESHENSU_URL = "/shensu/handle",
  SELECTGOOD_URL = "/shensu/selectGoodTiezi",
  XIAJIAGOOD_URL = "/shensu/deleteAndNotify",
  ALLGOODTIEZI_URL = "/shensu/goodTieziList",
  ALLFUWU_URL = "/shensu/list/user",
  DAFEN_URL = "/shensu/rate",
}

export const reqAllTiezi = (data: any) => request.post(API.ALL_TIEZI, data);

export const postTiezi = (data: any) => request.post(API.SUBMIT_TIEZI, data);

export const reqTieziDetail = (data: any) =>
  request.get(API.DETAIL_TIEZI + `/${data.id}`);
export const delTiezi = (data: any) =>
  request.delete(API.DEL_TIEZI + `/${data.id}`);

export const reqToupiao = (data: any) => request.post(API.TOUPIAO_URL, data);

export const reqDashang = (data: any) => request.post(API.DASHANG_URL, data);

export const reqAllPinglun = (data: any) =>
  request.post(API.ALLPINGLUN_URL, data);

export const postPinglun = (data: any) =>
  request.post(API.POSTPINGLLUN_URL, data);

export const delPinglun = (data: any) => {
  let a = request.post(API.DELPINGLUN_URL, data, {
    headers: {
      "Content-Type": " application/json ",
    },
  });
  return a;
};

export const reqDianZanPinglun = (data: any) =>
  request.post(API.DIANZANPINGLUN_URL, data);

export const postShensu = (data: any) =>
  request.post(API.SHENSU_URL, data, {
    headers: {
      "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
    },
  });

export const reqAllShensu = () => request.get(API.ALLSHENSU_URL);

export const handleShensu = (data: any) =>
  request.post(API.HANDLESHENSU_URL, data);

export const selectGoodTiezi = (data: any) =>
  request.post(API.SELECTGOOD_URL, data, {
    headers: {
      "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
    },
  });

export const xiajiaTiezi = (data: any) =>
  request.post(API.XIAJIAGOOD_URL, data, {
    headers: {
      "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
    },
  });

export const reqGoodTiezi = () => request.get(API.ALLGOODTIEZI_URL);

export const reqFuWu = (id: number) =>
  request.get(API.ALLFUWU_URL + `/${id}`, {
    headers: {
      "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
    },
  });

export const postDafen = (data: any) =>
  request.post(API.DAFEN_URL, data, {
    headers: {
      "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
    },
  });
