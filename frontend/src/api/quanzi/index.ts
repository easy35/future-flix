import request from "@/utils/request";

enum API {
  ALL_TIEZI = "/circles/all",
  USER_QUANZI = "/circles/user/all",
  JOIN_QUANZI = "/circles/join",
  QUIT_QUANZI = "/circles/exit",
}

export const reqUserQuanzi = (data: any) => {
  let a = request.post(API.USER_QUANZI, data, {
    headers: {
      "Content-Type": " application/json ",
    },
  });
  return a;
};

export const reqAllQuanzi = () => request.post(API.ALL_TIEZI);

export const reqJoinQuanzi = (data: any) => request.post(API.JOIN_QUANZI, data);

export const quitQuanzi = (data: any) =>
  request.delete(API.QUIT_QUANZI + `/${data.id}`);
