export const constantRoute = [
  {
    //登录成功以后展示数据的路由
    path: "/",
    component: () => import("@/layout/index.vue"),
    name: "layout",
    meta: {
      title: "帖子",
      hidden: false,
      icon: "Notebook",
    },
    redirect: "/home",
    children: [
      {
        path: "/home",
        component: () => import("@/views/home/index.vue"),
        meta: {
          title: "首页",
          hidden: false,
          icon: "HomeFilled",
        },
      },
      {
        path: "/submit",
        name: "submit",
        component: () => import("@/views/home/submit.vue"),
        meta: {
          title: "添加帖子", //菜单标题
          hidden: false, //代表路由标题在菜单中是否隐藏  true:隐藏 false:不隐藏
          icon: "DocumentAdd", //菜单文字左侧的图标,支持element-plus全部图标
        },
      },
      {
        path: "/manage",
        name: "manage",
        component: () => import("@/views/home/manage.vue"),
        meta: {
          title: "管理帖子", //菜单标题
          hidden: false, //代表路由标题在菜单中是否隐藏  true:隐藏 false:不隐藏
          icon: "Management", //菜单文字左侧的图标,支持element-plus全部图标
        },
      },
      {
        path: "/detail",
        name: "detail",
        component: () => import("@/views/home/detail.vue"),
        meta: {
          title: "帖子详情", //菜单标题
          hidden: true, //代表路由标题在菜单中是否隐藏  true:隐藏 false:不隐藏
          icon: "Management", //菜单文字左侧的图标,支持element-plus全部图标
        },
      },
      {
        path: "/dafen",
        name: "dafen",
        component: () => import("@/views/home/dafen.vue"),
        meta: {
          title: "打分服务", //菜单标题
          hidden: false, //代表路由标题在菜单中是否隐藏  true:隐藏 false:不隐藏
          icon: "Management", //菜单文字左侧的图标,支持element-plus全部图标
        },
      },
    ],
  },

  {
    path: "/login",
    name: "login",
    component: () => import("@/views/login/index.vue"),
    meta: {
      title: "登录", //菜单标题
      hidden: true, //代表路由标题在菜单中是否隐藏  true:隐藏 false:不隐藏
      icon: "Promotion", //菜单文字左侧的图标,支持element-plus全部图标
    },
  },
];

export const asyncRoute = {
  path: "/guanli",
  name: "guanli",
  component: () => import("@/layout/index.vue"),

  meta: {
    title: "管理员使用", //菜单标题
    hidden: false, //代表路由标题在菜单中是否隐藏  true:隐藏 false:不隐藏
    icon: "Promotion", //菜单文字左侧的图标,支持element-plus全部图标
  },
  redirect: "/guanli/jinping",
  children: [
    {
      path: "/guanli/jinping",
      name: "jinping",
      component: () => import("@/views/guanli/index.vue"),
      meta: {
        title: "精品或下架", //菜单标题
        hidden: false, //代表路由标题在菜单中是否隐藏  true:隐藏 false:不隐藏
        icon: "Management", //菜单文字左侧的图标,支持element-plus全部图标
      },
    },
    {
      path: "/guanli/shensu",
      name: "shensu",
      component: () => import("@/views/guanli/shensu.vue"),
      meta: {
        title: "处理申诉", //菜单标题
        hidden: false, //代表路由标题在菜单中是否隐藏  true:隐藏 false:不隐藏
        icon: "Management", //菜单文字左侧的图标,支持element-plus全部图标
      },
    },
  ],
};

export const anyRoute = [];
